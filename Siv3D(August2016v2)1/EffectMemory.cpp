#include "EffectMemory.h"

Effect* EffectMemory::effects;

void EffectMemory::addBombEffect(Vec2& pos)
{
	effects->add<Bomb>(pos, 34);
}

void EffectMemory::addDamageEffect(Vec2& pos)
{
	effects->add<Damage>(pos, 16);
}

void EffectMemory::update()
{
	effects->update();
}

EffectMemory::EffectMemory()
{
}

EffectMemory::~EffectMemory()
{
	effects->clear();
	effects = nullptr;
}

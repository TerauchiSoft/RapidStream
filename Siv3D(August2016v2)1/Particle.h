#pragma once
#include <Siv3d.hpp>

struct ParticleThisGame
{
	Vec2 pos;
	Vec2 vel;
};

struct Bomb : IEffect
{
	Array<ParticleThisGame> m_particles;

	Bomb(Vec2& pos, int count)
		: m_particles(count)
	{
		for (auto& particle : m_particles)
		{
			const Vec2 v = Circular(4.0f, Random(TwoPi));
			particle.pos = pos + v;
			double x = Random(100.0f, 500.0f) * (Random() <= 0.5f ? -1 : 1);
			double y = Random(100.0f, 500.0f) * (Random() <= 0.5f ? -1 : 1);
			particle.vel = Vec2(x, y);
		}
	}

	bool update(double t) override
	{
		for (const auto& particle : m_particles)
		{
			const Vec2 pos = particle.pos + particle.vel * t;

			Circle(pos, 18).draw(HSV(pos.y / 4.0, 0.6, 1.0).toColorF(1.0 - t));
		}

		return t < 1.0;
	}
};


struct Damage : IEffect
{
	Array<ParticleThisGame> m_particles;

	Damage(Vec2& pos, int count)
		: m_particles(count)
	{
		for (auto& particle : m_particles)
		{
			const Vec2 v = Circular(4.0f, Random(TwoPi));
			particle.pos = pos + v;
			particle.pos.x -= 60.0f;
			double x = Random(32.0f, 128.0f) * (Random() <= 0.5f ? -1 : 1);
			double y = Random(32.0f, 128.0f) * (Random() <= 0.5f ? -1 : 1);
			particle.vel = Vec2(x, y);
		}
	}

	bool update(double t) override
	{
		for (int i = 0; i < m_particles.size(); i++)
		{
			Vec2 pos = m_particles[i].pos + m_particles[i].vel * t;

			Rect(pos.x - t, pos.y, 20, 20).rotated(((i + 1) / 5) * t * TwoPi).draw(ColorF(1.0f, 0.5f * t, 0.5f, 1.0 - t));
		}

		return t < 1.0;
	}
};
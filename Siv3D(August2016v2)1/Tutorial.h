#pragma once
#include "GameSceneManager.h"
#include <Siv3D.hpp>
#include "Player_AnimControl.h"
#include <array>

class Tutorial : GameSceneManager
{
private:
	// override
	void init();
	void sceneDraw();
	void firstScene();

	/// <summary>
	/// リュートリアル文字列表示用。
	/// </summary>
	const Font font = Font(20);
	void ShowTutorial();

	const int SPAWNWAIT = 50;
	int spawnwait = SPAWNWAIT;

	/// <summary>
	/// 敵キャラの状態を更新する。
	/// </summary>
	void EnemiesRoutine();

	/// <summary>
	/// 敵キャラの情報。
	/// </summary>
	array<CharaParam, 90> cpms;

	/// <summary>
	/// マシュマロ君無限発生器。
	/// </summary>
	void enemySpawner();

	bool gotoGame = false;
	CharaParam pm;

	Player_AnimControl plycnt;
public:
	Scene Run();
	Tutorial(GameSound& snd, GimicDatas& g_data, CharacterDatas& c_data, BGChara& b_chr);
	~Tutorial();
};

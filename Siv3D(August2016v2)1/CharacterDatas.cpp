#include "CharacterDatas.h"

// キャラ定義はCharaAttribute.cppでしています。
ss::Player* CharacterDatas::GetDataPlayer(int chara_id)
{
	ss::Player* pp = new ss::Player();
	*pp = *param[chara_id].player;
	if (chara_id == 0)
		PlayerMemory::dataPushptrPlayer(pp);
	else if (chara_id == 1)
		PlayerMemory::dataPushptrKunai(pp);
	else
		PlayerMemory::dataPushptr(pp);

	reLoadAtIndex(chara_id);
	return pp;
}

// キャラ定義はCharaAttribute.cppでしています。
CharaParam CharacterDatas::GetData(int chara_id)
{
	CharaParam cp;
	cp.CharaId = chara_id;
	cp.acsVec = param[chara_id].acsVec;
	cp.initvec = param[chara_id].initvec;
	cp.posVec = param[chara_id].initvec;
	cp.maxVel = param[chara_id].maxVel;
	cp.player = new ss::Player();
	*cp.player = *param[chara_id].player;
	if (chara_id == 0)
		PlayerMemory::dataPushPlayer(cp);
	else if (chara_id == 1)
		PlayerMemory::dataPushKunai(cp);
	else
		PlayerMemory::dataPush(cp);

	reLoadAtIndex(chara_id);
	return cp;
}

CharacterDatas::CharacterDatas() {
	for (int i = 0; i < chara_Num; i++)
	{
		param[i] = CharaParam();
	}
}

void CharacterDatas::reLoadAtIndex(int idx)
{
	ss::Player* p = anm->ssplayer;
	CharacterAttribute atr = CharacterAttribute(idx);
	param[idx].initvec = s3d::Vec2(atr.GetPos().x, atr.GetPos().y);
	param[idx].posVec = param[idx].initvec;
	param[idx].acsVec = atr.GetAcs();
	param[idx].maxVel = Vec2(0, 0);
	param[idx].player = new ss::Player();
	*param[idx].player = *p;
	param[idx].player->setData(projectString[idx]);
	param[idx].player->setPosition(param[idx].initvec.x, param[idx].initvec.y);//表示位置を設定
	param[idx].player->play("Idle/Idle");							//アニメーション名をあらかじめ指定(ssae名/アニメーション名)
	param[idx].player->setScale(atr.GetScale(), atr.GetScale());	//スケール設定
	param[idx].player->setRotation(0.0f, 0.0f, 0.0f);				//回転を設定
	param[idx].player->setAlpha(atr.GetAlpha());					//透明度を設定
	param[idx].player->setFlip(false, false);
}

void CharacterDatas::reLoad()
{
	for (int i = 0; i < chara_Num; i++) {
		reLoadAtIndex(i);
	}

}

CharacterDatas::CharacterDatas(Animation &anim)
{
	anm = &anim;
	reLoad();
}

CharacterDatas::~CharacterDatas()
{
}

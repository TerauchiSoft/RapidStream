#include "GameSceneManager.h"


void GameSceneManager::init() {
	isFirst = true;
	isFadeComp = false;
	fade.init();
}

bool GameSceneManager::Fadein() {
	// フェードイン
	if (isFadeComp == false) {
		isFadeComp = fade.Fadein(colorTone);
		return false;
	}
	return true;
}

Scene GameSceneManager::Fadeout(bool isGotoFadeout, Scene nowsce, Scene gosce) {
	// フェードアウトしてシーン移行
	if (isGotoFadeout == true) {
		Scene sc = fade.Fadeout(colorTone, nowsce, gosce);
		if (sc == gosce) 
			return sc;
	}
	return nowsce;
}

void GameSceneManager::InitReference(GameSound& snd, GimicDatas& g_data, CharacterDatas& c_data)
{
	charas = &c_data;
	gimics = &g_data;
	sounds = &snd;
}

void GameSceneManager::InitReference(GameSound& snd, GimicDatas& g_data, CharacterDatas& c_data, BGChara& b_chr)
{
	charas = &c_data;
	gimics = &g_data;
	sounds = &snd;
	bg_chara = &b_chr;
}


GameSceneManager::GameSceneManager()
{
}


GameSceneManager::~GameSceneManager()
{
}

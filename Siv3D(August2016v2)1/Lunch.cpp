#include "Lunch.h"

void Lunch::init()
{
	isFirst = true;

	colorTone = Color(255, 255, 255);
	firstCount = FLAME_COUNT;
	secondCount = FLAME_COUNT;
}

void Lunch::firstScene() {
	if (isFirst) {
		PlayerMemory::ToFreeMemory();
		isFirst = false;
	}
}

void Lunch::ShowSpriteStudio()
{
	PlayerMemory::ToFreeMemory();
	ss::Player* p = gimics->GetDataPlayer(1);
	p->setLoop(1);
}

void Lunch::ShowTsSoft() 
{
	PlayerMemory::ToFreeMemory();
	ss::Player* p = gimics->GetDataPlayer(2);
	p->setLoop(1);
}

void Lunch::sceneDraw() {
	if (firstCount > 0) {
		if (firstCount == FLAME_COUNT)
			ShowSpriteStudio();

		firstCount--;
	}
	else if (secondCount > 0) {
		if (secondCount == FLAME_COUNT)
			ShowTsSoft();

		secondCount--;
	}
	PlayerMemory::ShowCharacter(colorTone);
}

Scene Lunch::Run()
{
	Scene sc = Scene::LUNCH;

	// 初期化の次の処理
	firstScene();

	sceneDraw();

	if (firstCount == 0 && secondCount == 0) {
		init();
		sc = Scene::TITLE;
	}

	return sc;
}

Lunch::Lunch(GameSound& snd, GimicDatas& g_data, CharacterDatas& c_data)
{
	InitReference(snd, g_data, c_data);
	init();
}


Lunch::~Lunch()
{
}

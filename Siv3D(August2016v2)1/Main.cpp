﻿#include <Siv3D.hpp>
#include "ssbpLib/SS5Player.h"
#include "Scene.h"
#include "Animation.h"
#include "CharacterDatas.h"
#include "GimicDatas.h"
#include "CharaParam.h"
#include "Game.h"
#include "GameSound.h"
#include "UISprite.h"
#include "Lunch.h"
#include "Score.h"
#include "Title.h"
#include "Tutorial.h"
#include "GameSceneManager.h"
#include "PlayerMemory.h"	//static

// prototype declaration
void OperationEnvironmentInitiarize();

/// <summary>
/// ゲームの現在のシーン
/// </summary>
Scene scene = Scene::LUNCH;

void OperationEnvironmentInitiarize()
{
	s3d::Window::Resize(1270, 720);
	Graphics::SetBackground(Palette::Black);
}


void Main()
{
	// Data Class Initiarize
	Animation animation;
	GameSound gamesound;
	UISprite uisprite;

	// Gimic Class Initiarize
	CharacterDatas chrdatas = CharacterDatas(animation);
	GimicDatas gimic = GimicDatas(animation);

	// Scene Class Initiarize
	BGChara b_chara;
	Lunch lunch = Lunch(gamesound, gimic, chrdatas);
	Title title = Title(gamesound, gimic, chrdatas, b_chara);
	Tutorial tutorial = Tutorial(gamesound, gimic, chrdatas, b_chara);
	Game game = Game(gamesound, gimic, chrdatas, b_chara);
	Score score = Score(gamesound, gimic, chrdatas);

	// Game Effect
	Effect effects;
	EffectMemory::effects = &effects;

	OperationEnvironmentInitiarize();
	while (System::Update())
	{
		switch (scene)
		{
		case Scene::LUNCH:
			scene = lunch.Run();
			break;
		case Scene::TITLE:
			scene = title.Run();
			break;
		case Scene::TUTORIAL:
			scene = tutorial.Run();
			break;
		case Scene::GAME:
			scene = game.Run();
			break;
		case Scene::SCORE:
			scene = score.Run();
			break;
		default:
			exit(0);
		}
	}
	PlayerMemory::ToFreeMemory();
}
#pragma once
#include "GameSceneManager.h"
#include "Game.h"

class Score : GameSceneManager
{
private:
	// override
	void init();
	void sceneDraw();
	void firstScene();

	Font font = Font(22);
	void SetHiScore();
	void ShowHiScore();

	const int FLAME_COUNT = 132;
	int firstCount = FLAME_COUNT;
	int secondCount = FLAME_COUNT;

	bool isgototitle = false;
public:
	static long scores[5];
	Scene Run();
	Score(GameSound& snd, GimicDatas& g_data, CharacterDatas& c_data);
	~Score();
};


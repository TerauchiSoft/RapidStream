#pragma once
#include "ssbpLib/SS5Player.h"
#include <Siv3D.hpp>
#include "EffectMemory.h"

/// <summary>
/// このクラスをゲーム中のキャラクターのパラメータとする。
/// </summary>
class CharaParam
{
private:
	const double gamespdcnfg = 0.75f;

	Point getBasePos();

	// --------------------------
	//
	//	AI命令。
	//

	Rect playerRectset();	// プレイヤーの当たり判定セット
	Rect katanaRectset();	// 刀の当たり判定セット
	Rect kunaiRectset(ss::Player* kunai);	// 苦無の当たり判定セット
	Rect getMineRect();		// キャラの当たり判定セット

	bool getPlayerCross(Rect mine);
	bool getKatanaCross(Rect mine);
	bool getKunaiCross(Rect mine);
	void charaDel();
	void playerDel();
	// chara
	void Kunai();
	void Mash();
	void Nyan(Vec2 &zikipos);
	void Mafies(Vec2 &zikipos);
	void Mash2();
	void Nyan2(Vec2 &zikipos);
	void Mafies2(Vec2 &zikipos);
	void update(Vec2 &zikipos);

	// ---------------------------

public:
	/// <summary>
	/// キャラID==0でプレイヤーかnullを表す。
	/// </summary>
	int CharaId = 0;

	/// <summary>
	/// 自機のポインタ。
	/// </summary>
	ss::Player* ziki = nullptr;

	/// <summary>
	/// 本当はPlayerMemoryに置きたかった。
	/// メモリ管理はPlayerMemoryに任せます。
	/// </summary>
	static vector<ss::Player*> kunais;

	/// <summary>
	/// アニメーションを操作するハンドラ。
	/// </summary>
	ss::Player* player = nullptr;

	/// <summary>
	/// 初期位置
	/// </summary>
	s3d::Vec2 initvec = s3d::Vec2(0, 0);

	/// <summary>
	/// 加速度、重力
	/// </summary>
	s3d::Vec2 acsVec = s3d::Vec2(0, 0);

	/// <summary>
	/// 速度
	/// </summary>
	s3d::Vec2 velVec = s3d::Vec2(0, 0);

	/// <summary>
	/// 位置
	/// </summary>
	s3d::Vec2 posVec = s3d::Vec2(0, 0);

	/// <summary>
	/// 更新角度
	/// </summary>
	int deg = 0;

	/// <summary>
	/// AIの速さ。
	/// </summary>
	double spd = 1.0f;

	void SetPosVec(Vec2 vec);

	/// <summary>
	/// 最高速度
	/// </summary>
	s3d::Vec2 maxVel = s3d::Vec2(0, 0);

	const float flamelate = 1.0f / 60.0f;

	void SetAISpeed(double spd);

	/// <summary>
	/// キャラごとの動き。
	/// AI更新、当たり判定などすべて。
	/// 返り値0:なにもない 1:スコア加算 2:苦無スコア加算 3:ダメージ
	/// </summary>
	int CharaAIMove(Vec2 &zikipos);

	CharaParam& operator= (const CharaParam& prm);
	CharaParam();
	~CharaParam();
};


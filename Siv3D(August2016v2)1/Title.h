#pragma once
#include "GameSceneManager.h"
#include <Siv3D.hpp>

class Title : GameSceneManager
{
private:
	// override
	void init();
	void sceneDraw();
	void firstScene();

	bool gotoGame = false;
	CharaParam pm;

public:
	Scene Run();
	Title(GameSound& snd, GimicDatas& g_data, CharacterDatas& c_data, BGChara& b_chr);
	~Title();
};


#include "GimicAttribute.h"


int GimicAttribute::GetAlpha()
{
	return alpha;
}

float GimicAttribute::GetScale()
{
	return scale;
}

Vec2 GimicAttribute::GetVec()
{
	return initvec;
}

GimicAttribute::GimicAttribute() {}

GimicAttribute::GimicAttribute(int gimic_num)
{
	vector<string> ss;
	switch (gimic_num)
	{
	case 0:	//player
		ss = { "Idle", "Idle_attack", "jump", "jump_efk", "run", "run_attack" };
		SetParam("Player", ss, 255, 1.0f, Vec2(0, 0));
		break;
	case 1: //sprite studio
		ss = { "Idle" };
		SetParam("sprite studio", ss, 255, 1.8f, Vec2(1270 / 2, 720 / 2));
		break;
	case 2: //TsSoft
		ss = { "Idle" };
		SetParam("TsSoft", ss, 255, 1.2f, Vec2(1270 / 2, 720 / 2));
		break;
	case 3: //Yane
		ss = { "Idle" };
		SetParam("Yane", ss, 255, 1.5f, Vec2(1270 / 2, 720 / 2));
		break;
	case 4: //RapidStream
		ss = { "Idle" };
		SetParam("RapidStream", ss, 255, 1.0f, Vec2(1270 / 2, 720 / 2));
		break;
	default:
		ss = { "Idle", "Idle_attack", "jump", "jump_efk", "run", "run_attack" };
		SetParam("Player", ss, 255, 1.0f, Vec2(0, 0));
		break;
	}
	ss.clear();
	ss.shrink_to_fit();
}

void GimicAttribute::SetParam(string name, vector<string> anims, int alp, float scl, Vec2 vec)
{
	gimic_Name = name;
	anime_State = anims;
	alpha = alp;
	scale = scl;
	initvec = vec;
}

GimicAttribute::~GimicAttribute()
{
	anime_State.clear();
	anime_State.shrink_to_fit();
}

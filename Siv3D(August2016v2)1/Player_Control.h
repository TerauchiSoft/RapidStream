#pragma once
#include <Siv3D.hpp>
#include "ssbpLib/SS5Player.h"
#include "Player_Input.h"

/// <summary>
/// ユーザーの入力をキーボード、パッドから読み込む。
/// 毎フレーム呼び出す。
/// </summary>
class Player_Control
{
private:
	// コントローラーもしくはキーボードの入力
	// 軸 x -1(左) 1(右), y -1(上) 1(下)
	Vec2 axis;
	// ボタンの入力もしくはキーボードの入力
	// 0ボタンジャンプ 1ボタン苦無
	vector<bool> button;

	/// <summary>
	/// 入力。
	/// </summary>
	void InputKey();
public:
	/// <summary>
	/// プレイヤーの入力を取得。毎フレーム呼び出そう。
	/// </summary>
	/// <returns></returns>
	Player_Input GetPlayerInput();

	Player_Control();
	~Player_Control();
};


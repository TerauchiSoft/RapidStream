#include "Player_Control.h"

void Player_Control::InputKey()
{
	// キーボード
	button[0] = Input::KeyZ.pressed || Input::KeyV.pressed;
	button[1] = Input::KeyX.pressed;
	button[2] = Input::KeyC.pressed;
	button[3] = Input::KeyUp.pressed;
	button[4] = Input::KeyR.pressed;

	bool lft = Input::KeyLeft.pressed;
	bool rit = Input::KeyRight.pressed;
	bool down = Input::KeyDown.pressed;
	bool up = Input::KeyUp.pressed;
	axis = Vec2(rit - lft, up - down);

	if (Gamepad(0).isConnected() == false || button[0] || button[1] || button[2] || button[3] || button[4] || lft || rit || down || up)
		return;

	// ボタン
	for (int i = 0; i < Gamepad(0).num_buttons - 1; ++i)
	{
		button[i] = Gamepad(0).button(i + 1).pressed;
	}
	axis.x = Gamepad(0).x;
	axis.y = Gamepad(0).y;
	if (axis.y < -0.1f) 
	{
		button[0] = true;
	}
}

Player_Input Player_Control::GetPlayerInput()
{
	InputKey();
	return Player_Input(button, axis);
}

Player_Control::Player_Control()
{
	for (int i = 0; i < Gamepad(0).num_buttons; i++)
	{
		button.push_back(false);
	}

	while (button.size() < 5) {
		button.push_back(false);
	}
}


Player_Control::~Player_Control()
{
}

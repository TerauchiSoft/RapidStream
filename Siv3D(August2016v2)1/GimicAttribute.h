#pragma once

#include "Array_temp.h"
#include <Siv3D.hpp>

using namespace std;

/// <summary>
/// キャラクターデータのマスターデータ的な役割。
/// Characterクラスから初期化時に呼び出されては消える。
/// </summary>
class GimicAttribute
{
private:
	/// <summary>
	/// ギミックの名前
	/// </summary>
	string gimic_Name;

	/// <summary>
	/// プロジェクト内の各アニメーションの名前はここで定義される。
	/// </summary>
	vector<string> anime_State;

	int alpha;
	float scale;
	Vec2 initvec;
	void SetParam(string name, vector<string> anims, int alp, float scl, Vec2 vec);
public:
	int GetAlpha();
	float GetScale();
	Vec2 GetVec();

	GimicAttribute();

	/// <summary>
	/// ギミックキャラの初期設定。
	/// インデックスごとにキャラを返す。
	/// </summary>
	/// <param name="gimic_num"></param>
	GimicAttribute(int gimic_num);
	~GimicAttribute();
};


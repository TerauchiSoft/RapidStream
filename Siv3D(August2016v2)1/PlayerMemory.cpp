#include "PlayerMemory.h"

std::vector<ss::Player*> PlayerMemory::memory;
ss::Player* PlayerMemory::player = nullptr;

void PlayerMemory::dataPushptr(ss::Player* p)
{
	memory.push_back(p);
}

void PlayerMemory::dataPushptrPlayer(ss::Player* p)
{
	memory.push_back(p);
	PlayerMemory::player = p;
}

void PlayerMemory::dataPushptrKunai(ss::Player* p)
{
	memory.push_back(p);
	CharaParam cp;
	cp.player = p;
	CharaParam::kunais.push_back(cp.player);
}

void PlayerMemory::dataPush(CharaParam& cp)
{
	memory.push_back(cp.player);
	cp.ziki = PlayerMemory::player;
}

void PlayerMemory::dataPushPlayer(CharaParam& cp)
{
	memory.push_back(cp.player);
	PlayerMemory::player = cp.player;
	cp.ziki = PlayerMemory::player;
}

void PlayerMemory::dataPushKunai(CharaParam& cp)
{
	memory.push_back(cp.player);
	CharaParam::kunais.push_back(cp.player);
	cp.ziki = PlayerMemory::player;
}

void PlayerMemory::ShowCharacter(Color& col)
{
	const float flamelate = 1.0f / 60.0f;
	for (int i = 0; i < memory.size(); i++) {
		memory[i]->setColor(col.r, col.g, col.b);
		memory[i]->update(flamelate);
		memory[i]->draw();
	}
}

void PlayerMemory::KunaiDataCheckAndDel(CharaParam& cp)
{
	for (int i = 0; i < CharaParam::kunais.size(); i++){
		if (cp.player == CharaParam::kunais[i]) {
			CharaParam::kunais.erase(CharaParam::kunais.begin() + i);
			return;
		}
	}
}

void PlayerMemory::DeleteCharacter(ss::Player* pl)
{
	for (int i = 0; i < memory.size(); i++) {
		if (memory[i] == pl) {
			delete(memory[i]);
			memory.erase(memory.begin() + i);
			break;
		}
	}
}

void PlayerMemory::CharaDeleteFromCP(CharaParam& cp)
{
	PlayerMemory::DeleteCharacter(cp.player);
	if (cp.CharaId == 1)
		KunaiDataCheckAndDel(cp);
	cp.CharaId = 0;
}

void PlayerMemory::ToFreeMemory()
{
	for (int i = 0; i < memory.size(); i++) {
		delete(memory[i]);
		memory[i] = nullptr;
	}
	player = nullptr;
	memory.clear();
	memory.shrink_to_fit();
	CharaParam::kunais.clear();
	CharaParam::kunais.shrink_to_fit();
}

PlayerMemory::~PlayerMemory()
{
	cout << "コピーインスタンスメモリ解放";
	ToFreeMemory();
}
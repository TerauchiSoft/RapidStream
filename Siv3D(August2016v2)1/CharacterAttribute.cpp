#include "CharacterAttribute.h"


int CharacterAttribute::GetAlpha()
{
	return alpha;
}

float CharacterAttribute::GetScale()
{
	return scale;
}


Vec2 CharacterAttribute::GetAcs()
{
	return acs;
}

Vec2 CharacterAttribute::GetPos() 
{
	return pos;
}

CharacterAttribute::CharacterAttribute() {}

CharacterAttribute::CharacterAttribute(int chara_num)
{
	vector<string> ss;
	switch (chara_num)// name, anims, spd, jmp, alp, scl, ac, mpos
	{
		case 0:	//player
			ss = { "Idle", "Idle_attack", "jump", "jump_efk", "run", "run_attack" };
			SetParam("Player", ss, 1.0f, 5.0f, 255, 0.3f, Vec2(0, -1.0f), Vec2(3.0f, -4.0f));
			break;
		case 1: //kunai
			ss = { "Idle" };
			SetParam("Kunai", ss, 1.0f, 1.0f, 255, 1.0f, Vec2(0.0f, 0.0f), Vec2(0, 600));
			break;
		case 2:	//mash
			ss = { "Idle" };
			SetParam("Mash", ss, 1.0f, 1.0f, 255, 1.0f, Vec2(-0.1f, 0.0f), Vec2(1500, 270 + 400 * Random()));
			break;
		case 3:	//nyan
			ss = { "Idle" };
			SetParam("Nyan", ss, 1.0f, 1.0f, 255, 1.0f, Vec2(-0.1f, 0.0f), Vec2(1500, 240 + 400 * Random()));
			break;
		case 4:	//mafies
			ss = { "Idle" };
			SetParam("Mafies", ss, 1.0f, 1.0f, 255, 1.0f, Vec2(-0.1f, 0.0f), Vec2(1500, 260 + 400 * Random()));
			break;
		case 5:	//mash
			ss = { "Idle" };
			SetParam("Mash2", ss, 1.0f, 1.0f, 255, 1.0f, Vec2(-0.1f, 0.0f), Vec2(1500, 230 + 400 * Random()));
			break;
		case 6:	//nyan
			ss = { "Idle" };
			SetParam("Nyan2", ss, 1.0f, 1.0f, 255, 1.0f, Vec2(-0.1f, 0.0f), Vec2(1500, 210 + 400 * Random()));
			break;
		case 7:	//mafies
			ss = { "Idle" };
			SetParam("Mafies2", ss, 1.0f, 1.0f, 255, 1.0f, Vec2(-0.1f, 0.0f), Vec2(1500, 280 + 400 * Random()));
			break;
		default:
			ss = { "Idle", "Idle_attack", "jump", "jump_efk", "run", "run_attack" };
			SetParam("Player", ss, 1.0f, 5.0f, 127, 0.2f, Vec2(-0.1f, 0.0f), Vec2(-3.0f, 0.0));
			break;
	}
	ss.clear();
	ss.shrink_to_fit();
}

void CharacterAttribute::SetParam(string name, vector<string> anims, 
								  float spd, float jmp, int alp, float scl, Vec2 ac, Vec2 mpos)
{
	chara_Name = name;
	anime_State = anims;
	chara_speed = spd;
	chara_jump = jmp;
	alpha = alp;
	scale = scl;
	acs = ac;
	pos = mpos;
}

CharacterAttribute::~CharacterAttribute()
{
	anime_State.clear();
	anime_State.shrink_to_fit();
}

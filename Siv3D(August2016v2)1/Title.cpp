#include "Title.h"

void Title::init() {
	isFirst = true;
	fade.init();
	isFadeComp = false;

	// this Scene
	gotoGame = false;
	pm = CharaParam();
	*bg_chara = BGChara();
}

void Title::firstScene() {
	if (isFirst == true) {
		PlayerMemory::ToFreeMemory();
		pm = gimics->GetData(4);
		isFirst = false;
	}
}

void Title::sceneDraw()
{
	bg_chara->showBG(false, colorTone);
	PlayerMemory::ShowCharacter(colorTone);
}

Scene Title::Run()
{
	Scene sc = Scene::TITLE;

	// フェードイン
	bool fadeincomp = Fadein();

	// フェードアウトしてシーン移行
	sc = Fadeout(gotoGame, sc, Scene::TUTORIAL);

	// 初期化の次の処理
	firstScene();

	sceneDraw();

	// 決定ボタンでシーン移行開始
	input = play_cont.GetPlayerInput();
	if (fadeincomp == true && gotoGame == false && input.buttons[0] == true) {
		fade.init();
		SoundAsset(L"Decision").play();
		gotoGame = true;
	}

	if (sc != Scene::TITLE)
		init();

	return sc;
}

Title::Title(GameSound& snd, GimicDatas& g_data, CharacterDatas& c_data, BGChara& b_chr)
{
	InitReference(snd, g_data, c_data, b_chr);
	init();
}


Title::~Title()
{
}

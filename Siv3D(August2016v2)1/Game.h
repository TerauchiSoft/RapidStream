#pragma once
#include "GameSceneManager.h"
#include <Siv3D.hpp>
#include "Player_AnimControl.h"
#include <array>
#include <random>

class Game : GameSceneManager
{
private:
	// override
	void init();
	void sceneDraw();
	void firstScene();

	int scalehp = 255;
	int hp = 255;
	double gamespeed = 1.0f;

	bool isGameOver = false;
	void HpDec();
	void ToGameOver();

	/// <summary>
	/// スコア文字列表示用。
	/// </summary>
	const Font font = Font(33);
	void ShowGameUI();

	/// <summary>
	/// GameOver用
	/// </summary>
	const Font fontgo = Font(99);
	int UIdeg = 0;

	const int SPAWNWAIT = 32;
	int spawnwait = SPAWNWAIT;

	void ActForAIAction(int act, int cid);

	/// <summary>
	/// 敵キャラの状態を更新する。
	/// </summary>
	void EnemiesRoutine();

	/// <summary>
	/// 敵キャラの情報。
	/// </summary>
	array<CharaParam, 90> cpms;

	/// <summary>
	/// 敵無限発生器。
	/// </summary>
	void enemySpawner();

	bool gotoGame = false;
	CharaParam pm;

	Player_AnimControl plycnt;
public:
	Scene Run();
	static long score;
	static long GetScore();

	Game(GameSound& snd, GimicDatas& g_data, CharacterDatas& c_data, BGChara& b_chr);
	~Game();
};

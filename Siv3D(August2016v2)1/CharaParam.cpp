#include "CharaParam.h"

vector<ss::Player*> CharaParam::kunais;

Point CharaParam::getBasePos()
{
	Point pt;
	auto state0 = ziki->getSpriteData(0)->_state;
	auto state1 = ziki->getSpriteData(1)->_state;
	pt.x = state0.x - state1.rect.size.width / 4;
	pt.y = state0.y - state1.rect.size.height / 2;
	return pt;
}

Rect CharaParam::playerRectset()
{
	if (ziki == nullptr)
		Rect(0, 0, 0, 0);

	// 基準軸
	Point pt = getBasePos();

	// 当たりサイズ
	Point ptsize;
	auto state = ziki->getSpriteData(1)->_state;
	ptsize.x = state.size_X / 2;
	ptsize.y = state.size_Y / 2;
	Rect plyrct;
	return plyrct.set(pt, ptsize);
}

Rect CharaParam::katanaRectset()
{
	// 自機がいないとき
	if (ziki == nullptr)
		return Rect(0, 0, 0, 0);

	// 刀のアニメ化チェック
	if (ziki->getPlayAnimeName() != "Idle_attack" || ziki->getFrameNo() > 4) {
		return Rect(0, 0, 0, 0);
	}

	// 基準軸
	Point pt = getBasePos();
	pt.y += ziki->getFrameNo() * 47 - 130;

	// 当たりサイズ
	Point ptsize;
	auto state = ziki->getSpriteData(1)->_state;
	ptsize.x = state.rect.size.width * 1.677f;
	ptsize.y = state.rect.size.height / 3.5f;
	Rect ktnrct;
	return ktnrct.set(pt, ptsize);
}

Rect CharaParam::kunaiRectset(ss::Player* kunai)
{
	auto state0 = kunai->getSpriteData(0)->_state;
	auto state1 = kunai->getSpriteData(1)->_state;
	Point pt;
	pt.x = state0.x - state1.rect.size.width / 2;
	pt.y = state0.y - state1.rect.size.height / 2;

	// 当たりサイズ
	Point ptsize;
	ptsize.x = state1.rect.size.width;
	ptsize.y = state1.rect.size.height;
	Rect ktnrct;
	return ktnrct.set(pt, ptsize);
}



void CharaParam::Kunai()
{
	posVec.x += 36 * spd * gamespdcnfg;
}

void CharaParam::Mash()
{
	posVec.x -= 12 * spd * gamespdcnfg;
}

void CharaParam::Nyan(Vec2 &zikipos)
{
	if (deg < 180)
		deg += 9 * spd * gamespdcnfg;
	else
		deg = 0;

	posVec.x = posVec.x - (spd * gamespdcnfg * 26 * Sin(deg * Pi / 180));
	if (Abs(posVec.y - zikipos.y) > 10.0f)
		posVec.y = posVec.y + spd * gamespdcnfg * 0.0068f * (zikipos.y - posVec.y);
}

void CharaParam::Mafies(Vec2 &zikipos)
{
	if (deg < 360)
		deg += 4 * gamespdcnfg;
	else
		deg = 0;

	posVec.x = posVec.x - (12 + spd * gamespdcnfg * 5 * Sin(deg * Pi / 180));
	posVec.y = posVec.y + (spd * gamespdcnfg * 7.5f * Random() * Sin(deg * Pi / 180));
}

void CharaParam::Mash2()
{
	posVec.x -= 12 * spd * gamespdcnfg;
}

void CharaParam::Nyan2(Vec2 &zikipos)
{
	if (deg < 180)
		deg += 9 * spd * gamespdcnfg;
	else
		deg = 0;

	posVec.x = posVec.x - (spd * gamespdcnfg * 26 * Sin(deg * Pi / 180));
	if (Abs(posVec.y - zikipos.y) > 10.0f)
		posVec.y = posVec.y + spd * gamespdcnfg * 0.0068f * (zikipos.y - posVec.y);
}

void CharaParam::Mafies2(Vec2 &zikipos)
{
	if (deg < 360)
		deg += 4 * gamespdcnfg;
	else
		deg = 0;

	posVec.x = posVec.x - (12 + spd * gamespdcnfg * 5 * Sin(deg * Pi / 180));
	posVec.y = posVec.y + (spd * gamespdcnfg * 7.5f * Random() * Sin(deg * Pi / 180));
}

void CharaParam::update(Vec2 &zikipos)
{
	switch(CharaId)
	{
	case 1:
		Kunai();
		break;
	case 2:
		Mash();
		break;
	case 3:
		Nyan(zikipos);
		break;
	case 4:
		Mafies(zikipos);
		break;
	case 5:
		Mash();
		break;
	case 6:
		Nyan(zikipos);
		break;
	case 7:
		Mafies(zikipos);
		break;
	default:
		break;
	}
}

Rect CharaParam::getMineRect()
{
	int x = player->getSpriteData(0)->_state.x - player->getSpriteData(1)->_state.size_X / 2;
	int y = player->getSpriteData(0)->_state.y - player->getSpriteData(1)->_state.size_Y / 2;
	int w = player->getSpriteData(1)->_state.rect.size.width;
	int h = player->getSpriteData(1)->_state.rect.size.height;
	Rect minrct = Rect(x, y, w, h);
	return minrct;
}

bool CharaParam::getPlayerCross(Rect mine)
{
	Rect plyrct = playerRectset();	// 自機の当たり判定
	bool cross = plyrct.intersects(mine);
	plyrct.draw();					//TODO:remove
	mine.draw(Color(128, 128, 128));//TODO:remove
	return cross;
}

bool CharaParam::getKatanaCross(Rect mine)
{
	Rect ktnrct = katanaRectset();
	bool cross = ktnrct.intersects(mine);
	ktnrct.draw(Color(255, 128, 128));//TODO:remove
	return cross;
}

bool CharaParam::getKunaiCross(Rect mine)
{
	for (int i = 0; i < kunais.size(); i++) {
		Rect knirct = kunaiRectset(kunais[i]);
		bool cross = knirct.intersects(mine);
		knirct.draw(Color(255, 128, 128));//TODO:remove
		if (cross == true) {
			kunais[i]->setPartVisible("NewCell", false);
			return cross;
		}
	}
	return false;
}

void CharaParam::charaDel()
{
	// ゲーム部に削除させる
	posVec.x = 9999;
}

void CharaParam::playerDel()
{

}

void CharaParam::SetAISpeed(double spd)
{

}

int CharaParam::CharaAIMove(Vec2 &zikipos)
{
	update(zikipos);
	player->setPosition(posVec.x, posVec.y);

	// プレイヤーか苦無のときは判定しない。
	if (player == ziki || CharaId == 1)
		return 0;

	// 判定
	Rect mineect = getMineRect();
	if (getPlayerCross(mineect)) {
		SoundAsset(L"Hit1").play();
		EffectMemory::addBombEffect(posVec);
		EffectMemory::addDamageEffect(posVec);
		charaDel();
		playerDel();
		return 3;
	}
	// 切るのは何でもできます
	if (getKatanaCross(mineect)) {
		SoundAsset(L"Ep1").play();
		EffectMemory::addBombEffect(posVec);
		charaDel();
		return 1;
	}
	// スライムは破壊できません
	if (CharaId != 2 && CharaId != 5 && getKunaiCross(mineect)) {
		SoundAsset(L"Ep2").play();
		EffectMemory::addBombEffect(posVec);
		charaDel();
		return 2;
	}
	return 0;
}

void CharaParam::SetPosVec(Vec2 vec)
{
	posVec = vec;
}

CharaParam& CharaParam::operator= (const CharaParam& prm)
{
	CharaId = prm.CharaId;
	player = prm.player;
	ziki = prm.ziki;
	kunais = prm.kunais;
	initvec = prm.initvec;
	acsVec = prm.acsVec;
	velVec = prm.velVec;
	posVec = prm.posVec;
	maxVel = prm.maxVel;
	return *this;
}

CharaParam::CharaParam()
{
}


CharaParam::~CharaParam()
{
}

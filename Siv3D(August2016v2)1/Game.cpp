#include "Game.h"

long Game::score = 0;

void Game::init() {
	isFirst = true;
	isFadeComp = false;
	fade.init();

	// this Scene
	gotoGame = false;
	spawnwait = SPAWNWAIT;
	plycnt.init();
	*bg_chara = BGChara();
	SoundAsset(L"BGM").stop();
	PlayerMemory::ToFreeMemory();
	return;
}

void Game::firstScene() {
	if (isFirst) {
		for (int i = 0; i < cpms.size(); i++)
			cpms[i].CharaId = 0;
		PlayerMemory::ToFreeMemory();

		SoundAsset(L"BGM").stop();
		SoundAsset(L"BGM").play();
		plycnt.SetCharacter(charas->GetDataPlayer(0));
		charas->reLoad();
		gimics->reLoad();
		isFirst = false;
		isGameOver = false;
		Game::score = 0;
		hp = 255;
		scalehp = 255;
		gamespeed = 1.0f;
	}
	return;
}

void Game::ShowGameUI() {
	if (UIdeg < 178) {
		if (hp < 128)
			UIdeg += 4;
		else
			UIdeg += 2;
	}
	else
		UIdeg = 0;

	auto degval = Sin(UIdeg * Pi / 180);
	if (hp < scalehp) {
		scalehp--;
		cout << "SCALEHPDEC";
	}
	double blcolor = 1;
	if (hp < 86)
		blcolor = 0.2f;

	TextureAsset(L"HP").scale(((double)scalehp / 256), 1.0f).draw(20, 20, Color(255, (degval * 127 + 128) * blcolor, 255 * blcolor, degval * 127 + 128));
	TextureAsset(L"HPLeft").draw(20, 20);
	TextureAsset(L"HPRight").draw(320, 20);
	TextureAsset(L"Score").draw(720, 60);
	String str = ToString(score);
	font(str).draw(940, 43);
	if (isGameOver == true)
		fontgo(L"Game Over").draw(280, 300, Color(192, 192, 32, degval * 255));

	return;
}

void Game::sceneDraw()
{
	bg_chara->showBG(true, colorTone);
	ShowGameUI();
	PlayerMemory::ShowCharacter(colorTone);
	EffectMemory::update();
	return;
}

void Game::HpDec()
{
	hp = hp - (Random() * 15 + 20);
	if (hp <= 0) {
		cout << "HP0!";
		hp = 0;
		ToGameOver();
		return;
	}
	return;
}

void Game::ToGameOver()
{
	PlayerMemory::player->setAlpha(0);
	isGameOver = true;
}

void Game::ActForAIAction(int act, int cid)
{
	switch (act)
	{
	case 0:
		return;
	case 1:
		score = score + 100;
		return;
	case 2:
		score = score + 200;
		return;
	case 3:
		HpDec();
	default:
		break;
	}
	return;
}

void Game::EnemiesRoutine()
{
	for (int i = 0; i < cpms.size(); ++i) {
		if (cpms[i].CharaId == 0)
			continue;

		// キャラ削除、移動、攻撃等
		int act = cpms[i].CharaAIMove(plycnt.GetposVec());
		ActForAIAction(act, i);

		// 画面外、透明時に削除
		if (cpms[i].posVec.x > 2000 || cpms[i].posVec.x < -100 ||
			cpms[i].posVec.y > 1400 || cpms[i].posVec.y < -100 || cpms[i].player->getSpriteData(2)->_state.isVisibled == false)
		{
			score -= 20;
			PlayerMemory::CharaDeleteFromCP(cpms[i]);
		}
	}
	return;
}

void Game::enemySpawner() {
	if (spawnwait > 0) {
		spawnwait--;
		return;
	}
	spawnwait = SPAWNWAIT;
	spawnwait /= gamespeed;
	gamespeed += 0.01f;

	for (int i = 0; i < cpms.size(); i++)
	{
		if (cpms[i].CharaId == 0)
		{
			cpms[i] = CharaParam();
			auto val = std::rand() % 3 + 2;
			cpms[i] = charas->GetData(val);
			cpms[i].player->setPosition(cpms[i].initvec.x, cpms[i].initvec.y);
			break;
		}
	}
}

Scene Game::Run()
{
	if (hp <= 0) {
		font(L"ぜろ！").draw(400, 400);
	}

	Scene sc = Scene::GAME;

	// フェードイン
	bool fadeincomp = Fadein();

	// フェードアウトしてシーン移行
	sc = Fadeout(gotoGame, sc, Scene::SCORE);

	// 初期化の次の処理
	firstScene();

	input = play_cont.GetPlayerInput();
	if (isGameOver == false) {
		plycnt.PlayerMove(input, cpms, charas);
		enemySpawner();
		EnemiesRoutine();
	}

	sceneDraw();

	// リセットボタンでシーン移行開始
	if (gotoGame == false && ((fadeincomp == true && gotoGame == false && input.buttons[4] == true) ||
		(isGameOver == true && input.buttons[0]))) {
		fade.init();
		SoundAsset(L"Decision").play();
		gotoGame = true;
	}

	if (sc != Scene::GAME)
		init();

	return sc;
}

long Game::GetScore()
{
	return Game::score;
}

Game::Game(GameSound& snd, GimicDatas& g_data, CharacterDatas& c_data, BGChara& b_chr)
{
	InitReference(snd, g_data, c_data, b_chr);
	init();
	return;
}


Game::~Game()
{
}

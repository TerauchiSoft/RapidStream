#pragma once
#include "ssbpLib/SS5Player.h"
#include "Player_Control.h"
#include <Siv3D.hpp>
#include "CharaParam.h"
#include "CharacterDatas.h"
#include <array>

/// <summary>
/// プレイヤーの行動はアニメの状態によって定義される。
/// </summary>
class Player_AnimControl
{
private:
	/// <summary>
	/// キャラパラメータの参照。
	/// </summary>
	CharaParam* cp;

	/// <summary>
	/// スプライトの参照。
	/// </summary>
	ss::Player* anim;

	/// <summary>
	///	基準の表示座標。
	/// </summary>
	const Vec2 basepos = Vec2(100, 640);

	/// <summary>
	/// 表示座標。(Siv3Dは下が＋を取るので表示するときは反転させよう。)
	/// </summary>
	Vec2 pos = Vec2(0, 0);

	/// <summary>
	/// 速度。
	/// </summary>
	Vec2 vel = Vec2(0, 0);

	/// <summary>
	/// 基準脚力。 = 5.0f
	/// </summary>
	const float forwardForce = 5.0f;

	/// <summary>
	/// 減速速度 = 0.8f
	/// </summary>
	const float resistanceVelocity = 0.8f;

	/// <summary>
	/// ジャンプ上昇中
	/// </summary>
	bool isjumped = false;

	/// <summary>
	/// 落下加速度。 = 1.0f
	/// </summary>
	const float gravity = 1.0f;

	/// <summary>
	/// 攻撃時にカウントセット
	/// </summary>
	const int TOIDLEWAIT = 18;

	/// <summary>
	/// 攻撃時にカウントセット、0でIdle再生。
	/// </summary>
	int toIdleWait = 0;

	/// <summary>
	/// 攻撃までのwait。攻撃開始からの時間。
	/// </summary>
	const int TOWAITFORATTACK = 21;

	/// <summary>
	/// 攻撃までのwait。攻撃開始からの時間。
	/// </summary>
	int toWaitForAttack = 0;


	// 入力に従って呼び出す。
	void GoForward(Player_Input& pi);
	void GoBackward(Player_Input& pi);
	void GoJump();
	void GoAttack();
	void GoThrow(array<CharaParam, 90>& cp, CharacterDatas* cd);

	void ToAppearKunai(array<CharaParam, 90>& cp, CharacterDatas* cd);

	/// <summary>
	/// 大げさなものではない。
	/// </summary>
	void PhysicalOperation();

	/// <summary>
	/// 座標を参照してアップデート。
	/// </summary>
	void AnimUpdate();

public:
	/// <summary>
	/// 入力に従って、座標や状態を変更する。
	/// </summary>
	/// <param name="pi"></param>
	void PlayerMove(Player_Input& pi, array<CharaParam, 90>& cp, CharacterDatas* cd);
	
	/// <summary>
	/// アニメ参照セット。
	/// </summary>
	/// <param name="pl"></param>
	void SetCharacter(ss::Player* charapm);

	Vec2 GetposVec();

	void init();

	Player_AnimControl();
	~Player_AnimControl();
};


#pragma once
#include <Siv3D.hpp>

struct Damage : IEffect
{
	struct Particle
	{
		Vec2 pos;
		Vec2 vel;
	};

	Array<Particle> m_particles;

	Damage(const Vec2& pos, int count)
		: m_particles(count)
	{
		for (auto& particle : m_particles)
		{
			const Vec2 v = Circular(4.0f, Random(TwoPi));
			particle.pos = pos + v;
			double x = Random(100.0f, 500.0f) * (Random() <= 0.5f ? -1 : 1);
			double y = Random(20.0f, 80.0f) * (Random() <= 0.5f ? -1 : 1);
			particle.vel = Vec2(x, y);
		}
	}

	bool update(double t) override
	{
		for (const auto& particle : m_particles)
		{
			const Vec2 pos = particle.pos + particle.vel * t;

			Rect(Point(pos, Point(40, 40))).rotated(t).draw((HSV(pos.y / 4.0, 0.6, 1.0).toColorF(1.0 - t)));
		}

		return t < 1.0;
	}
};
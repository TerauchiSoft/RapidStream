#include "GameSound.h"



GameSound::GameSound()
{
	SoundAsset::Register(L"Decision", L"Sound/Decision.wav");
	SoundAsset::Register(L"BGM", L"Sound/待ち人、通りゃんせ.mp3");
	SoundAsset(L"BGM").setLoop(true);
	SoundAsset::Register(L"Ep1", L"Sound/Explosion1.wav");
	SoundAsset::Register(L"Ep2", L"Sound/Explosion2.wav");
	SoundAsset::Register(L"Hit1", L"Sound/Hit1.wav");
	SoundAsset::Register(L"Hit2", L"Sound/Hit2.wav");
	SoundAsset::Register(L"Hit3", L"Sound/Hit3.wav");
	SoundAsset::Register(L"Hit4", L"Sound/Hit4.wav");
	SoundAsset::Register(L"Sride", L"Sound/sride.wav");
	SoundAsset::Register(L"Wave", L"Sound/Wave.wav");
}


GameSound::~GameSound()
{
}

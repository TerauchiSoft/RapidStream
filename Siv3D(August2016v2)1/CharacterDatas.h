#pragma once
#include "Animation.h"
#include "CharacterAttribute.h"
#include "Array_temp.h"
#include "CharaParam.h"
#include "PlayerMemory.h"
#include <Siv3D.hpp>

/// <summary>
/// キャラクターのデータの全てを格納するデータセンター。
/// </summary>
class CharacterDatas
{
private:
	/// <summary>
	/// 初期化用の参照。
	/// </summary>
	Animation* anm;

	/// <summary>
	/// キャラクターの数はここで指定。
	/// </summary>
	static const int chara_Num = 8;

	/// <summary>
	/// データの取得はこの配列変数にアクセスして行う。
	/// </summary>
	CharaParam param[chara_Num];

	/// <summary>
	/// 各インスタンスへのキー
	/// </summary>
	std::string projectString[chara_Num] = { "NewProject", "Kunai", "Mash", "Nyan", "Mafies", "Mash", "Nyan", "Mafies" };
public:
	/// <summary>
	/// プレイヤーのコピーのみ返す。
	/// </summary>
	/// <param name="chara_id"></param>
	/// <returns></returns>
	ss::Player* GetDataPlayer(int chara_id);

	/// <summary>
	/// ゲームで使用するためのキャラクターのデータを取得する。
	/// データのコピーを返す。
	/// </summary>
	/// <param name="chara_id"></param>
	/// <returns></returns>
	CharaParam GetData(int chara_id);

	/// <summary>
	/// GetDataでリソースコピーするごとにリロード。
	/// </summary>
	void reLoadAtIndex(int idx);

	/// <summary>
	/// Lunchでリロードする。
	/// </summary>
	void reLoad();

	CharacterDatas();
	CharacterDatas(Animation &anim);
	~CharacterDatas();
};


#include "UISprite.h"



UISprite::UISprite()
{
	TextureAsset::Register(L"HP", L"UI/HPBar.png");
	TextureAsset::Register(L"HPLeft", L"UI/HPLeft.png");
	TextureAsset::Register(L"HPRight", L"UI/HPRight.png");
	TextureAsset::Register(L"Score", L"UI/Score.png");
	TextureAsset::Register(L"ScoreBG", L"BG/Score.png");
}


UISprite::~UISprite()
{
}

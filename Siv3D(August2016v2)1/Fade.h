#pragma once
#include <Siv3D.hpp>
#include "Scene.h"

/// <summary>
/// 画面切り替え時に毎フレーム使用。
/// 呼び出し先で呼びっぱなしにする処理が必要。
/// </summary>
class Fade
{
private:
	const int TIM = 30;
	int tim = 30;
	bool isFirst = true;
public:
	/// <summary>
	/// フェードイン、フェードアウトを始めに実行する前に呼び出す。
	/// </summary>
	void init();

	/// <summary>
	/// 完了したらtrueを返す
	/// </summary>
	/// <param name="cl"></param>
	/// <returns></returns>
	bool Fadein(Color& cl);

	/// <summary>
	/// 完了したらgotoscを返す
	/// </summary>
	/// <param name="cl"></param>
	/// <param name="nowsc"></param>
	/// <param name="gotosc"></param>
	/// <returns></returns>
	Scene Fadeout(Color& cl, Scene nowsc, Scene gotosc);
	Fade();
	~Fade();
};


#pragma once
#include "ssbpLib/SS5Player.h"
#include "Animation.h"
#include "Array_temp.h"
#include "CharaParam.h"
#include "GimicAttribute.h"
#include "PlayerMemory.h"
#include <Siv3D.hpp>

/// <summary>
/// キャラクターのデータの全てを格納するデータセンター。
/// </summary>
class GimicDatas
{
private:
	/// <summary>
	/// リロード用参照。
	/// </summary>
	Animation* anm;

	/// <summary>
	/// ギミックの数はここで指定。
	/// </summary>
	static const int gimic_Num = 5;

	/// <summary>
	/// データの取得はこの配列変数にアクセスして行う。
	/// </summary>
	CharaParam g_param[gimic_Num];

	/// <summary>
	/// 各インスタンスへのキー
	/// </summary>
	std::string projectString[gimic_Num] = { "NewProject", "splash512", "TsSoft", "Yane", "RapidStream" };
public:
	/// <summary>
	/// プレイヤーのコピーのみ返す。
	/// </summary>
	/// <param name="chara_id"></param>
	/// <returns></returns>
	ss::Player* GetDataPlayer(int chara_id);

	/// <summary>
	/// ゲームで使用するためのキャラクターのデータを取得する。
	/// データのコピーを返す。
	/// </summary>
	/// <param name="chara_id"></param>
	/// <returns></returns>
	CharaParam GetData(int chara_id);

	/// <summary>
	/// GetDataでリソースコピーするごとにリロード。
	/// </summary>
	void reLoadAtIndex(int idx);

	/// <summary>
	/// Lunchで呼ぶ。
	/// </summary>
	void reLoad();

	GimicDatas();
	GimicDatas(Animation &anim);
	~GimicDatas();
};
#include "Tutorial.h"

void Tutorial::init() {
	isFirst = true;
	isFadeComp = false;
	fade.init();

	// this Scene
	gotoGame = false;
	spawnwait = SPAWNWAIT;
	plycnt.init();
	*bg_chara = BGChara();
	SoundAsset(L"BGM").stop();
	PlayerMemory::ToFreeMemory();
}

void Tutorial::firstScene() {
	if (isFirst) {
		for (int i = 0; i < cpms.size(); i++)
			cpms[i].CharaId = 0;
		PlayerMemory::ToFreeMemory();
		plycnt.SetCharacter(charas->GetDataPlayer(0));
		charas->reLoad();
		gimics->reLoad();
		isFirst = false;
	}
}

void Tutorial::ShowTutorial() {
	font(L"操作ボタン　1(↑):ジャンプ, 2:斬撃　　キーボード : Z(↑, V):ジャンプ, X:斬撃").draw(0, 0);
	font(L"操作ボタン　3:苦無投げ　　　　　　　　キーボード : C:苦無投げ").draw(0, 30);
	font(L"苦無で倒せない敵は刀で斬りまくれ！").draw(0, 60);
	font(L"Rキー または 5ボタンでゲームを開始。").draw(0, 90);
}

void Tutorial::sceneDraw()
{
	bg_chara->showBG(true, colorTone);
	ShowTutorial();
	PlayerMemory::ShowCharacter(colorTone);
	EffectMemory::update();
}

void Tutorial::EnemiesRoutine()
{
	for (int i = 0; i < cpms.size(); ++i) {
		if (cpms[i].CharaId == 0)
			continue;

		// キャラ削除、移動、攻撃等
		cpms[i].CharaAIMove(plycnt.GetposVec());

		// 画面外、透明時に削除
		if (cpms[i].posVec.x > 2000 || cpms[i].posVec.x < -100 ||
			cpms[i].posVec.y > 1200 || cpms[i].posVec.y < -100 || cpms[i].player->getSpriteData(2)->_state.isVisibled == false)
		{
			PlayerMemory::CharaDeleteFromCP(cpms[i]);
		}
	}
}

void Tutorial::enemySpawner() {
	if (spawnwait > 0) {
		spawnwait--;
		return;
	}
	spawnwait = SPAWNWAIT;

	for (int i = 0; i < cpms.size(); i++)
	{
		if (cpms[i].CharaId == 0)
		{
			cpms[i] = CharaParam();
			cpms[i] = charas->GetData(3);
			cpms[i].player->setPosition(cpms[i].initvec.x, cpms[i].initvec.y);
			break;
		}
	}
}

Scene Tutorial::Run()
{
	Scene sc = Scene::TUTORIAL;

	// フェードイン
	bool fadeincomp = Fadein();

	// フェードアウトしてシーン移行
	sc = Fadeout(gotoGame, sc, Scene::GAME);

	// 初期化の次の処理
	firstScene();

	input = play_cont.GetPlayerInput();
	plycnt.PlayerMove(input, cpms, charas);

	enemySpawner();

	sceneDraw();
	EnemiesRoutine();

	// リセットボタンでシーン移行開始
	if (fadeincomp == true && gotoGame == false && input.buttons[4] == true) {
		fade.init();
		SoundAsset(L"Decision").play();
		gotoGame = true;
	}

	if (sc != Scene::TUTORIAL)
		init();

	return sc;
}

Tutorial::Tutorial(GameSound& snd, GimicDatas& g_data, CharacterDatas& c_data, BGChara& b_chr)
{
	InitReference(snd, g_data, c_data, b_chr);
	init();
}


Tutorial::~Tutorial()
{
}

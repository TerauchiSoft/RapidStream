#include "GimicDatas.h"

// キャラ定義はGimicAttribute.cppでしています。
ss::Player* GimicDatas::GetDataPlayer(int gimic_id)
{
	ss::Player* pp = new ss::Player();
	*pp = *g_param[gimic_id].player;
	PlayerMemory::dataPushptr(pp);
	reLoadAtIndex(gimic_id);
	return pp;
}

// キャラ定義はGimicAttribute.cppでしています。
CharaParam GimicDatas::GetData(int gimic_id)
{
	CharaParam cp;
	cp.acsVec = g_param[gimic_id].acsVec;
	cp.initvec = g_param[gimic_id].initvec;
	cp.maxVel = g_param[gimic_id].maxVel;
	cp.player = new ss::Player();
	*cp.player = *g_param[gimic_id].player;
	PlayerMemory::dataPush(cp);
	reLoadAtIndex(gimic_id);
	return cp;
}

void GimicDatas::reLoadAtIndex(int idx)
{
	ss::Player* p = anm->ssplayer;
	GimicAttribute atr = GimicAttribute(idx);
	g_param[idx].initvec = atr.GetVec();
	g_param[idx].posVec = g_param[idx].initvec;
	g_param[idx].player = new ss::Player();
	*g_param[idx].player = *p;
	g_param[idx].player->setData(projectString[idx]);
	g_param[idx].player->play("Idle/Idle");							//アニメーション名をあらかじめ指定(ssae名/アニメーション名)
	g_param[idx].player->setPosition(g_param[idx].initvec.x, g_param[idx].initvec.y);							//表示位置を設定
	g_param[idx].player->setScale(atr.GetScale(), atr.GetScale());	//スケール設定
	g_param[idx].player->setRotation(0.0f, 0.0f, 0.0f);				//回転を設定
	g_param[idx].player->setAlpha(atr.GetAlpha());					//透明度を設定
	g_param[idx].player->setFlip(false, false);
}

void GimicDatas::reLoad()
{
	for (int i = 0; i < gimic_Num; i++) {
		reLoadAtIndex(i);
	}
}

GimicDatas::GimicDatas(Animation &anim)
{
	anm = &anim;
	reLoad();
}

GimicDatas::GimicDatas()
{
	{
		for (int i = 0; i < gimic_Num; i++)
		{
			g_param[i] = CharaParam();
		}
	}
}


GimicDatas::~GimicDatas()
{
}

#include "Animation.h"

Animation::Animation()
{
	// リソース登録
	resman = ss::ResourceManager::getInstance();
	for (int i = 0; i < array_length(resourceString); i++) {
		resman->addData(resourceString[i]);
	}

	// アニメーションの設定を読み込む。
	ss::Player* p;
	p = ss::Player::create(resman); 
	ssplayer = p;
}


Animation::~Animation()
{
	delete[](resman);
	resman = nullptr;
}
